﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Office_modeling.Tasks;

namespace Office_modeling
{
    class Timesheet : ICloneable
    {
        Dictionary<DateTime, TaskData> _tasks;

        public Timesheet()
        {
            _tasks = new Dictionary<DateTime, TaskData>();
        }

        protected Timesheet(Timesheet source)
        {
            _tasks = new Dictionary<DateTime, TaskData>(source._tasks);
        }

        public bool IsEmpty
        {
            get
            {
                return (_tasks.Count == 0);
            }
        }

        public void Add(DateTime time, TaskData task)
        {
            _tasks.Add(time, task);
        }

        public object Clone()
        {
           return new Timesheet(this);
        }

        public Dictionary<DateTime, TaskData> getTasks()
        {
            return new Dictionary<DateTime, TaskData>(_tasks);
        }

        private DateTime endTaskTime(DateTime startTime)
        {
            return startTime.AddHours(_tasks[startTime].Duration);
        }

        public KeyValuePair<DateTime, DateTime> LastTaskPeriod()
        {
            if (IsEmpty) throw new ArgumentOutOfRangeException("timeshhet is empty");
            KeyValuePair<DateTime, TaskData> lastTask = _tasks.Last();
            DateTime startTime = lastTask.Key;
            DateTime endTime = endTaskTime(startTime);
            return new KeyValuePair<DateTime,DateTime>(startTime,endTime);
        }

        public Dictionary<DateTime,TaskData> getTasksEnded(DateTime from,DateTime to)
        {
            Dictionary<DateTime,TaskData> res = new Dictionary<DateTime,TaskData>();
            foreach (DateTime startTask in _tasks.Keys)
            {
                DateTime endTime = endTaskTime(startTask);
                if ( (from < endTime)&&(endTime <= to) )
                {
                    res.Add(startTask,_tasks[startTask]);
                }
            }
            return res;
        }

        public uint TotalDuration()
        {
            uint total = 0;
            foreach (TaskData task in _tasks.Values)
            {
                total += task.Duration;
            }
            return total;
        }

        public double TasksCount()
        {
            return _tasks.Count;
        }
    }
}
