﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Office_modeling
{
    enum EmployerPosition { Director, Designer, Manager, Developer, QA, Cleaner, Booker };

    class PositionSheduler
    {

        static PositionSheduler _inst = new PositionSheduler();

        static public PositionSheduler Instance()
        {
            return _inst;
        }

        private PositionSheduler()
        {
            SetCompatibilityOfPositions();
            SetRates();
        }

        Dictionary<EmployerPosition, IRateClassification> rates;
        Dictionary<EmployerPosition, EmployerPosition[]> compatibility;

        private void SetRates()
        {
            rates = new Dictionary<EmployerPosition, IRateClassification>();
            int hoursPerWeek = WorkSheduler.Instance().MaxHoursPerWeek;
            rates.Add(EmployerPosition.Director, new FixedRate(10000, hoursPerWeek));
            rates.Add(EmployerPosition.Manager, new FixedRate(6000, hoursPerWeek));
            rates.Add(EmployerPosition.Booker, new FixedRate(8000, hoursPerWeek));
            rates.Add(EmployerPosition.Designer, new HourlyRate(15));
            rates.Add(EmployerPosition.Developer, new HourlyRate(20));
            rates.Add(EmployerPosition.QA, new HourlyRate(10));
            rates.Add(EmployerPosition.Cleaner, new HourlyRate(2));
        }

        private EmployerPosition[] getPositionsWithout(EmployerPosition[] list)
        {
            EmployerPosition[] allPositions = getAllPositions();
            List<EmployerPosition> res = new List<EmployerPosition>(allPositions);
            foreach (EmployerPosition pos in list)
            {
                res.Remove(pos);
            }
            return res.ToArray();
        }

        public EmployerPosition[] getAllPositions()
        {
            return (EmployerPosition[]) Enum.GetValues(EmployerPosition.Director.GetType());
        }

        private void SetCompatibilityOfPositions()
        {
            compatibility = new Dictionary<EmployerPosition, EmployerPosition[]>();
            compatibility.Add(EmployerPosition.Director, new EmployerPosition[] { EmployerPosition.Manager });
            compatibility.Add(EmployerPosition.Booker, new EmployerPosition[] { EmployerPosition.Manager });
            compatibility.Add(EmployerPosition.Manager, getPositionsWithout(new EmployerPosition[] { EmployerPosition.Cleaner }));
            EmployerPosition[] availablePositions = getPositionsWithout(new EmployerPosition[] { EmployerPosition.Cleaner, EmployerPosition.Director, EmployerPosition.Booker });
            compatibility.Add(EmployerPosition.Designer, availablePositions);
            compatibility.Add(EmployerPosition.Developer, availablePositions);
            compatibility.Add(EmployerPosition.QA, availablePositions);
            compatibility.Add(EmployerPosition.Cleaner, new EmployerPosition[] { });
        }

        public EmployerPosition[] getCompatible(EmployerPosition one)
        {
            return compatibility[one];
        }

        public EmployerPosition[] getCompatible(EmployerPosition[] list)
        {

            EmployerPosition[] compatible = (EmployerPosition[]) Enum.GetValues(EmployerPosition.Director.GetType());
            for ( int i = 0; i < list.Length; ++i)
            {
                compatible = (EmployerPosition[]) compatible.Intersect(getCompatible(list[i]));
            }
            return compatible;
        }

        public IRateClassification getRate(EmployerPosition position)
        {
            if (!rates.ContainsKey(position)) throw new IndexOutOfRangeException("rate for position undefined");
            return rates[position];
        }
    }
}
