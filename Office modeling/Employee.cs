﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Office_modeling.Tasks;

namespace Office_modeling
{
    class Employee
    {
        static uint lastid = 0;

        public static uint getNextId()
        {
            ++lastid;
            return lastid;
        }

        uint _id;
        List<EmployerPosition> _positions;
        bool _hasTask;

        int todayHours;
        int hoursInWeek;
        DateTime lastUpdate;

        Timesheet _timesheet;
        Payments _payments;

        public Employee(uint id,EmployerPosition position)
        {
            _id = id;
            _hasTask = false;
            _positions = new List<EmployerPosition>();
            _positions.Add(position);
            _payments = new Payments();
            _timesheet = new Timesheet();
            todayHours = 0;
            hoursInWeek = 0;
        }

        public bool appendtPosition(EmployerPosition position)
        {
            if (!_positions.Contains(position))
            {
                _positions.Add(position);
                return true;
            }
            return false;
        }

        public bool removePosition(EmployerPosition position)
        {
            return _positions.Remove(position);
        }

        public bool HasTask {
            get { return _hasTask;  }
            private set { _hasTask = value; }
        }

        public uint Id
        {
            get
            {
                return _id;
            }
        }

        public int TodayHours
        {
            get
            {
                return todayHours;
            }
        }

        public int HoursInWeek
        {
            get
            {
                return hoursInWeek;
            }
      }

        public List<EmployerPosition> Positions
        {
            get
            {
                return new List<EmployerPosition>(_positions);
            }
        }

        public Payments Payments
        {
            get
            {
                return _payments;
            }
        }

        public Timesheet Tinmesheet
        {
            get
            {
                return (Timesheet) _timesheet.Clone();
            }
        }

        public void appendTask(DateTime time,TaskData task)
        {
            if (HasTask) throw new ArgumentOutOfRangeException("Employee has task");
            HasTask = true;
            _timesheet.Add(time, task);
            //Console.WriteLine("Employee {5} ({6}:{7})=> added {0} ( ({1})h -> ${4}, for {3}, {2} )", task.Description, task.Duration, task.Priority, task.Position,  task.valueOfPayment(),_id,TodayHours,HoursInWeek);
        }

        public void Update(DateTime time)
        {
            if (lastUpdate.Day != time.Day)
            {
                todayHours = 0;
                if (time.DayOfWeek == DayOfWeek.Monday)
                {
                    hoursInWeek = 0;
                }
                lastUpdate = time.Date;
            }
            if ( HasTask )
            {
                KeyValuePair<DateTime, DateTime> _period = _timesheet.LastTaskPeriod();
                DateTime startTime = _period.Key;
                DateTime endTime = _period.Value;
                HasTask = (endTime < time);
                if (!HasTask)
                {
                    if ( startTime < time.Date)
                    {
                        startTime = time.Date;
                    }
                    todayHours += (endTime - startTime).Hours;
                    hoursInWeek += (endTime - startTime).Hours;
                }
            }
            lastUpdate = time;
        }

        public bool hasPosition(EmployerPosition position)
        {
           return _positions.Contains(position);
        }
    }
}
