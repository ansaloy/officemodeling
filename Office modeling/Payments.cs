﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Office_modeling
{
    class Payments
    {
        Dictionary<DateTime, double> _payments;

        public Payments() {
            _payments = new Dictionary<DateTime, double>();
        }

        public void Add(DateTime time,double value)
        {
            _payments.Add(time, value);
  //          Console.WriteLine("add payment ${0} at {1}", value, time);
        }

        public Dictionary<DateTime, double> GetPayments()
        {
            return _payments;
        }

        public double Total()
        {
            double total = 0;
            foreach (KeyValuePair<DateTime,double> curr in _payments)
            {
                total += curr.Value;
            }
            return total;
        }
    }
}
