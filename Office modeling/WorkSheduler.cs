﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Office_modeling.Tasks;
using Office_modeling.Utils;

namespace Office_modeling
{
    class WorkSheduler
    {
        static WorkSheduler _inst = new WorkSheduler();

        private WorkSheduler() {
            MaxHoursPerWeek = 40;
            MaxHoursPerDay = 8;
            _employers = new List<Employee>();
            _freelancers = new List<Employee>();
        }

        static public WorkSheduler Instance()
        {
            return _inst;
        }

        public int MaxHoursPerWeek { get; private set; }
        public int MaxHoursPerDay { get; private set; }
        List<Employee> _employers;
        List<Employee> _freelancers;


        private Employee AddRandomEmployee(EmployerPosition position)
        {
            Employee tmp = CreateRandomEmployee(position);
            _employers.Add(tmp);
            return tmp;
        }

        private Employee AddRandomFreelancer(EmployerPosition position)
        {
            Employee tmp = CreateRandomEmployee(position);
            _freelancers.Add(tmp);
            return tmp;
        }

        private static Employee CreateRandomEmployee(EmployerPosition position)
        {
            Employee tmp = new Employee(Employee.getNextId(), position);
            List<EmployerPosition> pos = new List<EmployerPosition>(PositionSheduler.Instance().getCompatible(position));
            Random rand = new Random();
            int count = rand.Next(pos.Count + 1);
            for (; count > 0; --count)
            {
                int idx = rand.Next(pos.Count);
                tmp.appendtPosition(pos[idx]);
                pos.RemoveAt(idx);
            }
            return tmp;
        }

        public void SetRandomEmployers(int count)
        {
            if (count < 3) throw new ArgumentOutOfRangeException("count < 3");
            AddRandomEmployee(EmployerPosition.Director);
            AddRandomEmployee(EmployerPosition.Booker);
            AddRandomEmployee(EmployerPosition.Manager);
            count -= 3;
            EmployerPosition[] pos = PositionSheduler.Instance().getAllPositions();
            Random rand = new Random();
            for (; count > 0; --count)
            {
                int idx = rand.Next(pos.Length);
                AddRandomEmployee(pos[idx]);
            }
        }

        public List<Employee> getEmployers()
        {
            return _employers;
        }

        public List<Employee> getFreelancers()
        {
            return _freelancers;
        }

        public void appendTask(TaskData task, bool availableForFreelancers)
        {
//           Console.WriteLine("append {0} ( ({1})h -> ${4}, for {3}, {2} )", task.Description, task.Duration, task.Priority, task.Position, task.valueOfPayment());
            List<Employee> available = getWorkers(task.Position, getEmployers(),task.Duration);
            if (available.Count == 0 &&  availableForFreelancers)
            {
                available = getWorkers(task.Position, getFreelancers(),task.Duration);
                if (available.Count == 0)
                {
                    Employee tmp = AddRandomFreelancer(task.Position);
                    available.Add(tmp);
                }
            }
            //if (available.Count == 0) throw new NotImplementedException("not available employers");
            foreach (Employee empl in available )
            {
                empl.appendTask((new TimeCounter()).getTime(), task);
            }
        }



        private List<Employee> getWorkers(EmployerPosition position, List<Employee> from, uint taskDuration)
        {
            List<Employee> res = new List<Employee>();
            foreach (Employee empl in from)
            {
                if (isAvailable(empl,taskDuration))
                {
                    if (empl.hasPosition(position))
                    {
                        res.Add(empl);
                    }
                }
            }
            return res;
        }

        private bool isAvailable(Employee empl,uint duration)
        {
            if ( !empl.HasTask )
            {
                if ( (empl.TodayHours + duration) <= MaxHoursPerDay)
                {
                    if ( (empl.HoursInWeek + duration) <= MaxHoursPerWeek )
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
