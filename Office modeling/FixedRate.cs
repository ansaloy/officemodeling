﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Office_modeling
{
    class FixedRate : IRateClassification
    {
        double _rate;
        int _hoursPerWeek;

        public FixedRate(double ratePerWeek,int hoursPerWeek)
        {
            _hoursPerWeek = hoursPerWeek;
            _rate = ratePerWeek / _hoursPerWeek;
        }

        public double Rate()
        {
            return _rate*_hoursPerWeek;
        }

        public double RatePerHour()
        {
            return _rate;
        }
    }
}
