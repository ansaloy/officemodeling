﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Office_modeling
{
    class HourlyRate : IRateClassification
    {
        double _rate;
        public HourlyRate(double rate)
        {
            _rate = rate;
        }

        public double Rate()
        {
            return RatePerHour();
        }

        public double RatePerHour()
        {
            return _rate;
        }
    }
}
