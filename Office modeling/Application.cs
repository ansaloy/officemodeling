﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Office_modeling.Tasks;
using Office_modeling.Utils;

namespace Office_modeling
{
    class Application : ITask
    {
        static TaskSheduler _sheduler = new TaskSheduler();

        readonly static int month = 1;
        readonly static int minEmployers = 10;
        readonly static int maxEmployers = 100;

        static void Main(string[] args)
        {
            TimeCounter.Set(month, 2016);

            WorkSheduler.Instance().SetRandomEmployers((new Random()).Next(minEmployers,maxEmployers+1));

            _sheduler.addTask(new Application());
            _sheduler.Run();
        }

        TimeCounter _counter = new TimeCounter();
        DateTime _lastDo = new DateTime(0);
        const int minTasksCount = 1;
        const int maxTasksCount = 5;
        Random rand = new Random();

        public void Do()
        {
            AddPaymentsTasks();
            if (month == _counter.getTime().Month)
            {
                AddTasksForEmployers();
                _sheduler.addTask(new UpdateTimer());
                _sheduler.addTask(new UpdateEmployersTasks());
                _sheduler.addTask(this);
            }
            else {
                _sheduler.addTask(new Summary());
            }
            _lastDo = _counter.getTime();
        }

        private void AddPaymentsTasks()
        {
            if (_lastDo.Ticks == 0) return;
            if (_lastDo.Day != _counter.getTime().Day)
            {
                _sheduler.addTask(new PaymentsForFreelancers(_counter.getTime()));
                if ( _lastDo.DayOfWeek != _counter.getTime().DayOfWeek )
                {
                    if ( _counter.getTime().DayOfWeek == DayOfWeek.Monday )
                    {
                        _sheduler.addTask(new PaymentsForEmployers(_counter.getTime(),7));
                    }
                }
            }
        }

        private void AddTasksForEmployers()
        {
            int count = rand.Next(minTasksCount, maxTasksCount);
            TaskForEmployee[] tasks = new TaskForEmployee[count];
            for (int i = 0; i < count; ++i)
            {
                tasks[i] = TaskForEmployersFact.getRandomTask();
            }
            Array.Sort(tasks);
            Array.Reverse(tasks);
            foreach (ITask task in tasks)
            {
                _sheduler.addTask(task);
            }
        }
    }
}