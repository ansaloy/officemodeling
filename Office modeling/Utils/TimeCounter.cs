﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Office_modeling.Utils
{
    class TimeCounter
    {
        static DateTime _timer = DateTime.Now;
        static readonly int _step = 60*60; //1 hour in seconds 

        static public void Set(int month, int year)
        {
           _timer = new DateTime(year, month, 1);
        }

        public bool isWeekend()
        {
            return ((_timer.DayOfWeek == DayOfWeek.Sunday) || (_timer.DayOfWeek == DayOfWeek.Saturday));
        }

        public bool isLastDayOfMonth()
        {
            return (_timer.Month != _timer.AddDays(1).Month);
        }

        public bool isLastDayOfWeek()
        {
            return (_timer.DayOfWeek == DayOfWeek.Sunday);
        }

        public DateTime getTime()
        {
            return _timer;
        }

        public void Update()
        {
            _timer = _timer.AddSeconds(_step);
        }
    }
}
