﻿using System;
using System.Collections.Generic;
using Office_modeling.Tasks;

namespace Office_modeling
{
    internal class PaymentsForEmployers : ITask
    {
        private DateTime date;
        private int days;

        public PaymentsForEmployers(DateTime date,int periodInDays)
        {
            this.date = date;
            this.days = periodInDays;
        }

        private double paymentCoefficient(DateTime time)
        {
            double coeff = 1;
            if ((time.DayOfWeek == DayOfWeek.Sunday) || (time.DayOfWeek != DayOfWeek.Saturday))
                coeff = 2;
            return coeff;
        }

        public void Do()
        {
            List<Employee> employers = WorkSheduler.Instance().getEmployers();
            foreach (Employee empl in employers)
            {
                Dictionary<DateTime, TaskData> tasks = empl.Tinmesheet.getTasksEnded(date.AddDays(-1), date);
                double payment = 0;
                IRateClassification _rate;
                foreach (KeyValuePair<DateTime, TaskData> task in tasks)
                {
                    _rate = PositionSheduler.Instance().getRate(task.Value.Position);
                    if (!(_rate is HourlyRate))
                        payment += task.Value.Duration * _rate.RatePerHour()  * paymentCoefficient(task.Key);
                }
                payment = addFixedPayments(empl);
//                Console.WriteLine("Employer {0} payment ${1} at {2}", empl.Id, payment, date);
                if (payment != 0)
                    empl.Payments.Add(date,payment);
            }
        }

        private double addFixedPayments(Employee empl)
        {
            double payment = 0;
            foreach (EmployerPosition pos in empl.Positions)
            {
                IRateClassification rate = PositionSheduler.Instance().getRate(pos);
                if (rate is FixedRate)
                    payment += rate.Rate();
            }
            return payment;
        }
    }
}