﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Office_modeling.Utils;

namespace Office_modeling.Tasks
{
    class UpdateTimer : ITask
    {
        TimeCounter _counter = new TimeCounter();

        public void Do()
        {
            _counter.Update();
            //Console.WriteLine("ticks " + _counter.getTime().ToString("dd.MM.yyyy HH.mm.ss"));
        }
    }
}
