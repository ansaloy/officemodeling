﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Office_modeling.Utils;

namespace Office_modeling.Tasks
{
    class UpdateEmployersTasks : ITask
    {
        public void Do()
        {
            TimeCounter tc = new TimeCounter();
            Update(WorkSheduler.Instance().getEmployers(),tc);
            Update(WorkSheduler.Instance().getFreelancers(),tc);
        }

        private static void Update(List<Employee> workers, TimeCounter tc)
        {
            foreach (Employee empl in workers)
            {
                empl.Update(tc.getTime());
            }
        }
    }
}
