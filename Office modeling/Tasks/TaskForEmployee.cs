﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Office_modeling.Tasks
{
    class TaskForEmployee : TaskData, ITask, IComparable
    {
        bool _availableForFrelancers = true;

        public TaskForEmployee(string description, uint duration, EmployerPosition position, TaskPriority priority, bool availableForFrelancers = true) : base(priority, position, description, duration)
        {
            _availableForFrelancers = availableForFrelancers;
        }

        public void Do()
        {
            WorkSheduler.Instance().appendTask(this, _availableForFrelancers);
        }

        public int CompareTo(object obj)
        {
            int result = 1;
            TaskForEmployee right = obj as TaskForEmployee;
            if (this.Priority < right.Priority)
            {
                result = -1;
            }
            else if(Priority == right.Priority)
            {
                if ( this.valueOfPayment() < right.valueOfPayment())
                {
                    result = -1;
                } 
                else if (this.valueOfPayment() == right.valueOfPayment())
                {
                    result = 0;
                }
            }
            return result;
        }

    }
}
