﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Office_modeling.Tasks
{
    class TaskForEmployersFact
    {
        static Random rand = new Random();

        public static TaskForEmployee getRandomTask()
        {
            int idx = rand.Next(0, _createFuncs.Count);
            return _createFuncs[idx]();
        }

        delegate TaskForEmployee CreateTaskFunc();
        static List<CreateTaskFunc> _createFuncs = new List<CreateTaskFunc>();

        static TaskForEmployersFact()
        {
            _createFuncs.Add(WriteCode);
            _createFuncs.Add(DrawLayout);
            _createFuncs.Add(TestProgram);
            _createFuncs.Add(SellServices);
            _createFuncs.Add(MakeStatements);
            _createFuncs.Add(ClearOffice);
        }

        protected static TaskPriority getRandomPriority()
        {
            Type enumType = TaskPriority.High.GetType();
            int nameCounts = (Enum.GetNames(enumType)).Length;
            return (TaskPriority)rand.Next(nameCounts);
        }

        protected static uint getRandomDuration()
        {
            return (uint) rand.Next(1,3);
        }

        public static TaskForEmployee WriteCode()
        {
            return new TaskForEmployee("write code", getRandomDuration(), EmployerPosition.Developer, getRandomPriority());
        }

        public static TaskForEmployee DrawLayout()
        {
            return new TaskForEmployee("draw a layout", getRandomDuration(), EmployerPosition.Designer, getRandomPriority());
        }

        public static TaskForEmployee TestProgram()
        {
            return new TaskForEmployee("test a programm", getRandomDuration(), EmployerPosition.QA, getRandomPriority());
        }

        public static TaskForEmployee SellServices()
        {
            return new TaskForEmployee("sell services", getRandomDuration(), EmployerPosition.Manager, getRandomPriority());
        }

        public static TaskForEmployee MakeStatements()
        {
            return new TaskForEmployee("make statements", getRandomDuration(), EmployerPosition.Booker, getRandomPriority());
        }

        public static TaskForEmployee ClearOffice()
        {
            return new TaskForEmployee("clear office", getRandomDuration(), EmployerPosition.Cleaner, getRandomPriority(),false);
        }
    }
}
