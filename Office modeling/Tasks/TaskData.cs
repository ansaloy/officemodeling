﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Office_modeling.Tasks
{
    enum TaskPriority { Low, Medium, High };

    class TaskData
    {
        TaskPriority _priority;
        EmployerPosition _position;
        string _description;
        uint _duration;

        public TaskData(TaskPriority priority, EmployerPosition position, string description, uint duration)
        {
            this._priority = priority;
            this._position = position;
            this._description = description;
            this._duration = duration;
        }

        public double valueOfPayment()
        {
            double paymentPerHour = PositionSheduler.Instance().getRate(Position).RatePerHour();
            return Duration * paymentPerHour;

        }

        public TaskPriority Priority {
            get { return _priority; }
        }

        public EmployerPosition Position {
            get { return _position; }
        }

        public string Description  {
            get  { return _description;  }
        }

        public uint Duration  {
            get  { return _duration; }
        }
    }
}
