﻿using System;
using System.Collections.Generic;
using Office_modeling.Tasks;

namespace Office_modeling
{
    internal class PaymentsForFreelancers : ITask
    {
        private DateTime date;

        public PaymentsForFreelancers(DateTime dateTime)
        {
            this.date = dateTime;
        }

        private double paymentCoefficient(DateTime time)
        {
            double coeff = 1;
            if ((time.DayOfWeek == DayOfWeek.Sunday) || (time.DayOfWeek != DayOfWeek.Saturday))
                coeff = 2;
            return coeff;
        }

        public void Do()
        {
            List<Employee> peoples = WorkSheduler.Instance().getFreelancers();
            foreach (Employee emp in peoples)
            {
                Dictionary<DateTime,TaskData> tasks = emp.Tinmesheet.getTasksEnded(date.AddDays(-1), date);
                double payment = 0;
                IRateClassification rate;
                foreach (KeyValuePair<DateTime,TaskData> task in tasks)
                {
                    rate = PositionSheduler.Instance().getRate(task.Value.Position);
                    payment += task.Value.Duration * rate.RatePerHour() * paymentCoefficient(task.Key);
                }
//                Console.WriteLine("Freelancer {0} payment ${1} at {2}", emp.Id, payment, date);
                if (payment != 0) 
                    emp.Payments.Add(date, payment);
            }
        }
    }
}