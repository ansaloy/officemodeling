﻿using System;

namespace Office_modeling.Tasks
{
    class NullTask : ITask
    {
        public void Do()
        {
            Console.WriteLine("Null Task");
        }
    }
}