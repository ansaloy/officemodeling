﻿using System;
using System.Collections.Generic;
using System.IO;

using Office_modeling.Utils;

namespace Office_modeling.Tasks
{
    class Summary : ITask
    {
        TimeCounter _counter = new TimeCounter();
        const String filename = "summary.txt";

        public Summary()
        {
        }

        public uint TotalDurations(Employee empl)
        {
            uint duration = 0;
            foreach (TaskData task in empl.Tinmesheet.getTasks().Values)
            {
                duration += task.Duration;
            }
            return duration;
        }

        private void WriteTotal(ref StringWriter os, String predicate, List<Employee> workers)
        {
            uint totalDuration = 0;
            double totalPaid = 0;
            foreach (Employee empl in workers)
            {
                totalDuration += empl.Tinmesheet.TotalDuration();
                totalPaid += empl.Payments.Total();
            }
            os.WriteLine(predicate);
            os.WriteLine("    task durations : {0} (hours)", totalDuration);
            os.WriteLine("    paid           : {0} (dollars)", totalPaid);
            os.WriteLine("");
        }

        public void Do()
        {
            List<Employee> employers = WorkSheduler.Instance().getEmployers();
            List<Employee> freelancers = WorkSheduler.Instance().getFreelancers();

            StringWriter os = new StringWriter();

            List<Employee> total = new List<Employee>(employers);
            total.AddRange(freelancers);

            WriteTotal(ref os, "Total summary", total);
            WriteTotal(ref os, string.Format("Employers - {0}", employers.Count), employers);
            WriteTotal(ref os, string.Format("Freelancers - {0}", freelancers.Count), freelancers);

            os.WriteLine(" InPersonate data");
            WriteInpersonate(ref os, "Employer", employers);
            WriteInpersonate(ref os, "Freelancer", freelancers);

            File.WriteAllText(filename, os.ToString());
        }

        private void WriteInpersonate(ref StringWriter os, string predicate, List<Employee> workers)
        {
            foreach (Employee empl in workers)
            {
                os.WriteLine(predicate + " {0}", empl.Id);
                os.WriteLine("    tasks          : {0}", empl.Tinmesheet.TasksCount());
                os.WriteLine("    task durations : {0}", empl.Tinmesheet.TotalDuration());
                os.WriteLine("    paid           : {0}", empl.Payments.Total());
                os.WriteLine("");
            }
        }
    }
}