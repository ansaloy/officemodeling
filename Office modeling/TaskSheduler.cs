﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Office_modeling.Tasks;

namespace Office_modeling
{
    class TaskSheduler
    {
        List<ITask> taskList = new List<ITask>();

        public void addTask(ITask task)
        {
            taskList.Add(task);
        }

        public void Run()
        {
            while (taskList.Count > 0)
            {
                ITask tmp = taskList.First();
                taskList.RemoveAt(0);
                tmp.Do();
            }
        }

        public ITask[] Clear()
        {
            ITask[] unprocessed = taskList.ToArray();
            taskList = new List<ITask>();
            return unprocessed;
        }
    }
}
